import streamlit as st
from keytotext import pipeline
from trainer import trainer

model = trainer()

st. title("JobAdTerminator")

col1, col2 = st.columns(2, gap = "large")

with col1:
    st.header("Job Title")
    job_title = st.text_input("Enter job title")
    if job_title:
        st.write("Current job title: ", job_title)
    else:
        st.write("No job title provided")

    st.header("Pensum")
    pensum = st.slider("Wähle das Pensum in %", min_value = 20, max_value = 100, step = 10)
    pensum_percentage = str(pensum)+ "%"

    st.header("Industry")
    industries = st.multiselect(
        "Please select the appropriate industries",
        ["Gastronomie und Hotellerie",
         "Detailhandel",
         "Gesundheitswesen",
         "Informatik",
         "Baugewerbe",
         "Öffentliche Verwaltung",
         "Bildung",
         "Grosshandel",
         "Finanzdienstleistungen"])
    if not industries:
        st.write("Keine Branche ausgewählt")

    st.header("Job Type")
    job_type = st.selectbox(
        "Please select the job type",
        ("Standard", "Führung", "Lehrstelle"))
    if job_type:
        st.write(job_type, " selected")
    else:
        st.write("Kein Job Typ ausgewählt")

    st.header("Fähigkeiten und Kompetenzen")
    job_skill = st.text_area("Bitte gebe die Fähigkeiten und Kompetenzen ein (Wörter mit Komma trennen)")
    job_skill_list = job_skill.split(", ")
    st.write(job_skill_list)

    button = st.button("Generiere Jobanzeige")


with col2:
    st.header("Generated Output")
    if button:
        list = []
        if job_type != 'Standard':
            list.append(job_type)
        list.append(job_title)
        list.extend(industries)
        list.extend(job_skill_list)
        list.append(pensum)
        model.load_model(".\model_mt5-base")
        output = model.predict(list, use_gpu=False)
        st.write(output)
